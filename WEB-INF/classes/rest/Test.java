package rest;


import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;

import javax.ws.rs.core.Response;

import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.json.bind.JsonbException;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import rest.model.IModel;


@Path("/")
public class Test {
 
 @Inject
 IModel model;
 
 
 @POST   
 @Path("/test")
 @Consumes("application/json")
 @Produces("application/json")
 public Response test(String studentsJSON) 
 {            
   Jsonb jsonb = JsonbBuilder.create();          
   ArrayList<Product> products;      
   String resultJSON;
   try {  
  
	 products= model.run();	  	  
	  resultJSON = jsonb.toJson(products);	  	 
   }
   catch (JsonbException e) {
    return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();	             
   }
   catch (Exception e) {
    return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();	             
   }    
   return Response.ok(resultJSON).build();           
 }

  
@GET 
@Path("/gettable")
@Consumes("application/json")
@Produces("application/json")
public Response getTable(String studentsJSON) 
{            
  Jsonb jsonb = JsonbBuilder.create();          
  ArrayList<Product> products;      
  String resultJSON;
  try {  
 
  products= model.run();	  	  
   resultJSON = jsonb.toJson(products);	  	 
  }
  catch (JsonbException e) {
   return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();	             
  }
  catch (Exception e) {
   return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();	             
  }    
  return Response.ok(resultJSON).build();           
}

@PUT 
@Path("/putrow")
@Consumes("application/json")
@Produces("application/json")
public Response putRow(@QueryParam("name") String Name, @QueryParam("cost")double cost) 
{            
  Jsonb jsonb = JsonbBuilder.create();          
        
  String resultJSON;
  try {  
 
  model.put(Name,cost);	  	  
   resultJSON = jsonb.toJson("ok");	  	 
  }
  catch (JsonbException e) {
   return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();	             
  }
  catch (Exception e) {
   return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();	             
  }    
  return Response.ok(resultJSON).build();           
}

@DELETE
@Path("/deleterow")
@Consumes("application/json")
@Produces("application/json")
public Response deleteRow(@QueryParam("id") int id) 
{            
  Jsonb jsonb = JsonbBuilder.create();          
  ArrayList<Product> products;      
  String resultJSON;
  try {  
 
   model.delete(id);	  	  
   resultJSON = jsonb.toJson("ok");	  	 
  }
  catch (JsonbException e) {
   return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();	             
  }
  catch (Exception e) {
   return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();	             
  }    
  return Response.ok(resultJSON).build();           
}
 
}

