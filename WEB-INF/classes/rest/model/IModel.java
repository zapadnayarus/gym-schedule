package rest.model;

import rest.Product;
import java.util.List;
import java.util.ArrayList;
public interface IModel  {
  ArrayList<Product> run() throws Exception;
  void put(String Name,double cost) throws Exception;
  void delete(int id) throws Exception;
}